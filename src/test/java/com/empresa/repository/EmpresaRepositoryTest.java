package com.empresa.repository;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.empresa.Dataset;
import com.empresa.TesteSpring;
import com.empresa.model.EEmpresa;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Testa a interface pública de {@link EmpresaRepository}.
 * 
 * @author vinicius
 */
public class EmpresaRepositoryTest extends TesteSpring {

	@Autowired
	private EmpresaRepository empresaRepository;
	
	/**
	 * Testa os cenários comuns de utilização para {@link EmpresaRepository#findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase(String, String, String, String, org.springframework.data.domain.Pageable)}
	 */
	@Test
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testFindByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase(){
		Page<EEmpresa> resultado = null;
		
		/* Caso 01: somente CNPJ existente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("01117277000161", "%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(1, resultado.getTotalElements());
		
		/* Caso 02: somente CNPJ inexistente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("12345", "%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(0, resultado.getTotalElements());
		
		/* Caso 03: somente razão social parcial existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%OO%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
		/* Caso 04: somente razão social parcial inexistente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%xil%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(0, resultado.getTotalElements());
		
		/* Caso 05: somente razão social completa existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "FaCeBooK%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(1, resultado.getTotalElements());
		
		/* Caso 06: somente município parcial existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","sÃo%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
		/* Caso 07: somente município parcial inexistente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","sOO%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(0, resultado.getTotalElements());
		
		/* Caso 08: somente município completo existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","sÃo PauLo%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(1, resultado.getTotalElements());
		
		/* Caso 09: somente UF parcial existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","%" ,"s%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
		/* Caso 10: somente UF completa existente e com caixa diferente do armazenado */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","%" ,"sP%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
		/* Caso 11: somente UF inexistente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","%" ,"mG%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(0, resultado.getTotalElements());
		
		/* Caso 12: CNPJ existente e razão social parcial existente para mais de um registro */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("01117277000161", "%OO%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(1, resultado.getTotalElements());
		
		/* Caso 13: CNPJ inexistente e razão social parcial existente para mais de um registro */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("12345", "%OO%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(0, resultado.getTotalElements());
		
		/* Caso 14: CNPJ existente e razão social completa existente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("01117277000161", "gOOglE%","%" ,"%", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(1, resultado.getTotalElements());
		
		/* Caso 15: razão social parcial existente and UF completa existente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%oo%","%" ,"sP", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
		/* Caso 16: munícipio parcial existente e UF completa existente */
		resultado = this.empresaRepository.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase("%", "%","sÃo%" ,"SP", new PageRequest(0, 10));
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(2, resultado.getTotalElements());
		
	}
}
