package com.empresa.repository;

import org.springframework.data.repository.CrudRepository;

import com.empresa.model.EEndereco;

/**
 * {@link CrudRepository} para {@link EEndereco}.
 * 
 * @author vinicius
 * @since X
 */
public interface EnderecoRepository extends CrudRepository<EEndereco, Long> {
	//
}
