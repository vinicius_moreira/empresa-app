package com.empresa.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.empresa.model.EEmpresa;
import com.empresa.repository.EmpresaRepository;
import com.empresa.service.EmpresaDTO;
import com.empresa.service.EmpresaService;

/**
 * Implementação padrão de {@link EmpresaService}
 * 
 * @author vinicius
 */
@Service("com.empresa.service.impl.EmpresaServiceImpl")
@Transactional
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	private ConversorEntidades conversor;
	
	@Autowired
	private EmpresaRepository repository;
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public EmpresaDTO salvar(EmpresaDTO empresa) throws NullPointerException {
		Validate.notNull(empresa);
		
		EEmpresa entidade = null;
		
		if(empresa.getId() != null){
			entidade = this.repository.findOne(empresa.getId());
		}
		else{
			entidade = new EEmpresa(empresa.getRazao(), empresa.getCnpj(), empresa.getTelp());
		}
		
		this.conversor.popularEmpresa(empresa, entidade);
		
		entidade = this.repository.save(entidade);
		this.em.flush();
		this.em.clear();
		
		entidade = this.repository.findOne(entidade.getId());
		this.conversor.popularDTO(entidade, empresa);
		return empresa;
	}

	@Override
	public EmpresaDTO buscar(Long id) throws NullPointerException {
		Validate.notNull(id,"Id null !");
		
		EEmpresa entidade = this.repository.findOne(id);
		EmpresaDTO dto = null;
		
		if(entidade != null){
			dto = new EmpresaDTO();
			this.conversor.popularDTO(entidade, dto);
		}
		
		return dto;
	}

	@Override
	public List<EmpresaDTO> buscar(EmpresaDTO dadosBusca, Pageable requisicaoPaginada)
			throws NullPointerException {
		Validate.notNull(dadosBusca,"dadosBusca null !");
		
		String cnpj = dadosBusca.getCnpj() != null && dadosBusca.getCnpj().length() > 0 ? dadosBusca.getCnpj() : "%";
		String razaoSocial = dadosBusca.getRazao() != null && dadosBusca.getRazao().length() > 0 ? dadosBusca.getRazao()+"%" : "%";
		String municipio = dadosBusca.getMunicipio() != null && dadosBusca.getMunicipio().length() > 0 ? dadosBusca.getMunicipio()+"%" : "%";
		String uf = dadosBusca.getUf() != null && dadosBusca.getUf().length() > 0 ? dadosBusca.getUf() : "%";
		
		Page<EEmpresa> resultado = this.repository
		.findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase
			(cnpj, razaoSocial, municipio, uf, requisicaoPaginada);
		
		List<EEmpresa> empresas = resultado.getContent();
		List<EmpresaDTO> dtos = new ArrayList<EmpresaDTO>();
		this.popular(empresas, dtos);
		return dtos;
	}
	
	private void popular(Collection<EEmpresa> entidades, Collection<EmpresaDTO> dtos){
	
		if(entidades == null || entidades.isEmpty()){
			return;
		}
		
		for(EEmpresa empresa : entidades){
			EmpresaDTO dto = new EmpresaDTO();
			this.conversor.popularDTO(empresa,dto);
			dtos.add(dto);
		}
		
	}

	@Override
	public void remover(Long id) throws NullPointerException {
		Validate.notNull(id,"id null !");
		
		this.repository.delete(id);
		this.em.flush();
		this.em.clear();
	}

}
