package com.empresa.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.junit.Test;

import com.empresa.Dataset;
import com.empresa.TesteSpring;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

/**
 * Testa a interface pública de {@link EEmpresa}.
 * 
 * @author vinicius
 */
public class EEmpresaTest extends TesteSpring {
	
	@Test(expected=NullPointerException.class)
	public void testNew$razaoNull(){
		new EEmpresa(null, "25914176000180", "+551266667777");
	}
	
	@Test(expected=NullPointerException.class)
	public void testNew$cnpjNull(){
		new EEmpresa("Nova Empresa", null, "+551266667777");
	}
	
	@Test(expected=NullPointerException.class)
	public void testNew$telPrincipalNull(){
		new EEmpresa("Nova Empresa", "25914176000180", null);
	}
	
	@Test(expected=PersistenceException.class)
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@ExpectedDatabase(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testPersist$razaoSocialNull(){
		EEmpresa empresa = this.criarEmpresaCompleta(true);
		empresa.setRazaoSocial(null);
		this.persist(empresa);
	}
	
	@Test(expected=PersistenceException.class)
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@ExpectedDatabase(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testPersist$cnpjNull(){
		EEmpresa empresa = this.criarEmpresaCompleta(true);
		empresa.setCnpj(null);
		this.persist(empresa);
	}
	
	@Test(expected=PersistenceException.class)
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@ExpectedDatabase(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testPersist$telefonePrincipalNull(){
		EEmpresa empresa = this.criarEmpresaCompleta(true);
		empresa.setTelefonePrincipal(null);
		this.persist(empresa);
	}
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01_02)
	@ExpectedDatabase(Dataset.CENARIO_01_02_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testRemove$matrizEFiliais(){
		this.em.remove(this.em.find(EEmpresa.class, 1L));
		this.flushAndClear();
	}
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01_02)
	@ExpectedDatabase(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testRemove$filial(){
		
		EEmpresa google = this.em.find(EEmpresa.class, 3L);
		this.em.remove(google);
		this.flushAndClear();
	}
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testPersist$matrizCompleta(){
		
		EEmpresa empresa = this.criarEmpresaCompleta(true);
		this.persist(empresa);
		assertTrue(empresa.isMatriz());
	}
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testPersist$filialCompleta(){
		
		EEmpresa google = this.em.find(EEmpresa.class, 1L);
		EEmpresa empresa = this.criarEmpresaCompleta(true);
		empresa.setMatriz(google);
		empresa = this.persist(empresa);
		
		empresa = this.em.find(EEmpresa.class, empresa.getId());
		assertNotNull(empresa.getMatriz());
		assertEquals(google.getId(), empresa.getMatriz().getId());
		assertTrue(empresa.isFilial());
		
		google = this.em.find(EEmpresa.class, 1L);
		assertEquals(google.getFiliais().size(),1);
		assertTrue(google.getFiliais().contains(empresa));
		assertTrue(google.isMatriz());
	}
	
	private EEmpresa persist(EEmpresa empresa){
		this.em.persist(empresa);
		this.flushAndClear();
		return empresa;
	}
	
	private void flushAndClear(){
		this.em.flush();
		this.em.clear();
	}
	
	private EEmpresa criarEmpresaCompleta(boolean comEndereco){
		EEmpresa empresa = new EEmpresa("Nova Empresa", "25914176000180", "+551266667777");
		
		empresa.setRazaoSocial("Nova Empresa");
		empresa.setCnpj("25914176000180");
		
		if(comEndereco){
			EEndereco endereco = new EEndereco(empresa, "Rua Z", 14, "São Sebastião", "SP");
			endereco.setComplemento("Edífico Colinas, Sala 12");
			empresa.setEndereco(endereco);
		}
		
		empresa.setTelefonePrincipal("+551266667777");
		empresa.setTelefoneSecundario("+5512333344444");
		empresa.setFax("+551255559999");
		empresa.setEmail("novaempresa@x.com");
		
		return empresa;
	}
	

}
