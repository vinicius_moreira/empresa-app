app.controller("inclusaoEmpresaController", function ($scope, $http, $location, $compile) {
	
	$scope.tel_mascara9 = "+99 99 9 9999-9999";
	$scope.tel_mascara8 = "+99 99 9999-9999";
	
	$scope.telp_mascara = $scope.tel_mascara8;
	$scope.tels_mascara = $scope.tel_mascara8;
	$scope.fax_mascara = $scope.tel_mascara8;

	$scope.alterarMascara = function(event, contato){
		
                mascara = $scope.tel_mascara8;

		if(event.target.checked){
			mascara = $scope.tel_mascara9;
		}		

		if(contato == 'telp'){
			$scope.telp_mascara = mascara;
		}
		else if(contato == 'tels'){
			$scope.tels_mascara = mascara;
		}
		else if(contato == 'fax'){
		    $scope.fax_mascara = mascara;
		}
		
	};


	$scope.iniciarVotacao = function(){
		
		$http.get($location.absUrl()+"listarMaisVotados").success(function(response){

			// armazenando os livros
			$scope.livros=response;
			prepararCandidatos();
			
			// obtendo a página à ser injetada
			$http.get($location.absUrl()+"iniciarVotacao").success(function(response){
				$("#div_votacao").html($compile(response)($scope)); 
			});
		});
	};
	
	$scope.votar = function(naoEscolhido,divEscolhido){
		$("#"+divEscolhido+"_img").animate({width:'-=20px',height:'-=20px'},100);
		$("#"+divEscolhido+"_img").animate({width:'+=20px',height:'+=20px'},100);
		
		removerLivro(naoEscolhido);
		verificarVotacao();

        if($scope.votacaoPendente){
			prepararCandidatos();
		}
		else{
		    contabilizarVoto();
		}
	};
	
	var contabilizarVoto = function contabilizarVoto(){
		
		$.ajax({
		    method: "POST",
		    url: $location.absUrl()+"contabilizarVoto",
		    data: ({
		        "livroId" : $scope.livros[0].id
		    }),
		    success: function(response){
		    	$("#div_votacao").html($compile(response)($scope)); 
		   }
		});
		
	};
	
	var verificarVotacao = function(){
		if($scope.livros.length==1){
			$scope.votacaoPendente=false;
		}
	};
	
	var prepararCandidatos = function(){
		
		if($scope.livros.length>1){
			embaralhar($scope.livros);
			$scope.livro1=$scope.livros[0];
			$scope.livro2=$scope.livros[1];
		}
	};
	
	var removerLivro = function removerLivro(removido){
		var indice = $scope.livros.indexOf(removido)
  		$scope.livros.splice(indice, 1); 
    }
	
	var embaralhar = function embaralhar(arr){
		for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
	    return arr;
	};
	
});