package com.empresa.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Responsável por inicializar a aplicação, carregando os contextos do Spring. Substitui o arquivo "web.xml".<br/><br/>
 * Funciona somente em containers que suportam a API Servlet 3.0.  
 * 
 * @author vinicius
 * @since X
 */
public class ApplicationInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container)
			throws ServletException {
		
		/* Configurando o contexto Root do Spring */
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(RootConfig.class);
		rootContext.register(PersistenceConfig.class);
		container.addListener(new ContextLoaderListener(rootContext));
		
		/* Configurando o contexto Web do Spring */
		AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
		webContext.register(WebConfig.class);
		
		ServletRegistration.Dynamic dispatcherServlet = container.addServlet("frontController", new DispatcherServlet(webContext));
		dispatcherServlet.setLoadOnStartup(1);
		dispatcherServlet.addMapping("/");
	}
}