package com.empresa.controller.empresa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controle responsável pela página inicial da aplicação.
 * 
 * @author vinicius
 */
@Controller("com.empresa.controller.empresa.WelcomeController")
@RequestMapping("")
public class WelcomeController {

	/**
	 * @return
	 * 		redireciona para o controlador responsável pela view considerada inicial.
	 */
	@RequestMapping("")
	public String welcome(){
		return "redirect:/empresas/listagem";
	}
}
