package com.empresa.service;

import java.io.Serializable;

/**
 * DataTransferObject que representa uma empresa.
 * 
 * @author vinicius
 */
public class EmpresaDTO implements Serializable{

	/** */
	private static final long serialVersionUID = 3899951066224684195L;
	private Long id;
	private String razao;
	private String cnpj;
	private String logradouro;
	private Integer numero;
	private String complemento;
	private String municipio;
	private String uf;
	private String telp;
	private String tels;
	private String fax;
	private String email;
	private byte[] logo;
	private byte[] contrato;
	
	public EmpresaDTO(){
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 * 		razão social.
	 */
	public String getRazao() {
		return razao;
	}
	
	public void setRazao(String razaoSocial) {
		this.razao = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipo) {
		this.municipio = municipo;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	/**
	 * @return
	 * 		telefone principal.
	 */
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telPrincipal) {
		this.telp = telPrincipal;
	}
	
	/**
	 * @return
	 * 		telefone secundario.
	 */
	public String getTels() {
		return tels;
	}
	public void setTels(String telSecundario) {
		this.tels = telSecundario;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public byte[] getLogo() {
		return logo;
	}
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
	
	/**
	 * @return
	 * 		contrato social.
	 */
	public byte[] getContrato() {
		return contrato;
	}
	
	public void setContrato(byte[] contrato) {
		this.contrato = contrato;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaDTO other = (EmpresaDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "EmpresaDTO [id=" + id + ", razaoSocial=" + razao
				+ ", cnpj=" + cnpj + ", uf=" + uf + ", telPrincipal="
				+ telp + "]";
	}
	
}