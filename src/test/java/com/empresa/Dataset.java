package com.empresa;

/**
 * Classe abstrata que representa os datasets utilizados pelos testes unitários.
 * 
 * @author vinicius
 */
public abstract class Dataset {

	private static final String BASE_DATASETS = "classpath:datasets/";
	
	/** Banco de dados vazio */
	public static final String VAZIO = BASE_DATASETS + "vazio.xml";
	
	/** Cenário: Empresa 01(id=1, rs='Google'), Empresa 02 (id=2, rs='Facebook') */ 
	public static final String CENARIO_01 = BASE_DATASETS + "cenario-01.xml";
	
	/** Variação do Cenário 01: Empresa 03 (id=3, rs='Nova Empresa') adicionada */ 
	public static final String CENARIO_01_01 = BASE_DATASETS + "cenario-01-01.xml";

	/** Variação do Cenário 01: Empresa 03 (id=3, rs='Nova Empresa') adicionada como filial da Empresa 01 */ 
	public static final String CENARIO_01_02 = BASE_DATASETS + "cenario-01-02.xml";

	/** Variação do Cenário 01-02: Empresa 01 é removida */ 
	public static final String CENARIO_01_02_01 = BASE_DATASETS + "cenario-01-02-01.xml";

	/** Variação do Cenário 01: Empresa 01 atualizada */ 
	public static final String CENARIO_01_03 = BASE_DATASETS + "cenario-01-03.xml";
}
