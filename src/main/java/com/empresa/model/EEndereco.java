package com.empresa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.Validate;

@Entity
@Table(name="EMP_ENDERECOS")
public class EEndereco implements Serializable {

	/** */
	private static final long serialVersionUID = -3393598800001825916L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="EMPRESA_ID", nullable = false, unique = true)
	private EEmpresa empresa;
	
	@Column(nullable=false, length=150)
	private String logradouro;
	
	@Column(nullable=false, length=10)
	private Integer numero;
	
	@Column(length=150)
	private String complemento;
	
	@Column(length=150, nullable=false)
	private String municipio;
	
	@Column(nullable=false, length=2)
	private String uf;
	
	/**
	 * Construtor padrão.
	 * @deprecated
	 * 		deve ser utilizado somente pelo JPA.
	 */
	@SuppressWarnings("unused")
	private EEndereco(){
		//
	}

	/**
	 * Constrói uma nova instância de {@link EEndereco}.
	 * @param empresa
	 * @param logradouro
	 * @param numero
	 * @param municipio
	 * @param uf
	 * @throws NullPointerException
	 * 		caso algum argumento seja nulo
	 */
	public EEndereco(EEmpresa empresa, String logradouro, Integer numero,
			String municipio, String uf) throws NullPointerException {
		Validate.notNull(empresa,"'Empresa' obrigatório");
		Validate.notNull(logradouro,"'Logradouro' obrigatório");
		Validate.notNull(numero,"'Número' obrigatório");
		Validate.notNull(municipio,"'Município' obrigatório");
		Validate.notNull(uf,"'UF' obrigatória");
		this.empresa = empresa;
		this.logradouro = logradouro;
		this.numero = numero;
		this.municipio = municipio;
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public EEmpresa getEmpresa() {
		return empresa;
	}
	
	/**
	 * @param empresa
	 * 		só deve ser utilizado pelo JPA.
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private void setEmpresa(EEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EEndereco other = (EEndereco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
