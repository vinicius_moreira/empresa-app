package com.empresa.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.charset.Charset;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import com.empresa.Dataset;
import com.empresa.TesteSpring;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

/**
 * Testa a interface pública de {@link EmpresaControllerTest}.
 * @author vinicius
 *
 */
public class EmpresaControllerTest extends TesteSpring {
	
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),                       
            Charset.forName("utf8")                    
            );
	
	@Test
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testAtualizar() throws Exception {
		
		/* Caso 01: existente */
		ResultActions result = this.mockMvc.perform(get("/empresas/").param("id", "1"));
		
		result.andExpect(status().isOk());
		result.andExpect(view().name("empresa/inclusao"));
		
		/* Caso 02: inexistente */
		result = this.mockMvc.perform(get("/empresas/").param("id", "80"));
		
		result.andExpect(status().isOk());
		result.andExpect(view().name("empresa/inclusao"));
	}
	
	@Test
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testExcluir() throws Exception {
		ResultActions result = this.mockMvc.perform(delete("/empresas/1"));
		result.andExpect(status().isOk());
		result.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		result.andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void testListar() throws Exception {
		ResultActions result = this.mockMvc.perform(get("/empresas/listagem"));
		
		result.andExpect(status().isOk());
		result.andExpect(view().name("empresa/listagem"));
	}
	
	@Test
	public void testIncluir() throws Exception {
		ResultActions result = this.mockMvc.perform(get("/empresas/"));
		
		result.andExpect(status().isOk());
		result.andExpect(view().name("empresa/inclusao"));
	}
}
