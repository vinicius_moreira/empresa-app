package com.empresa;

import java.io.InputStream;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.core.io.Resource;

import com.github.springtestdbunit.dataset.AbstractDataSetLoader;

/**
 * Substitui os valores '[null]' nos datasets por valores <code>null<code> reais.
 * 
 * @author vinicius
 * @since X
 */
public class CustomDatasetLoader extends AbstractDataSetLoader {

	@Override
	protected IDataSet createDataSet(Resource resource) throws Exception {
		FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		InputStream inputStream = resource.getInputStream();
		try {
			FlatXmlDataSet dataset = builder.build(inputStream);
			ReplacementDataSet decoratedDataset = new ReplacementDataSet(dataset);
			decoratedDataset.addReplacementObject("[null]", null);
			return decoratedDataset;
			
		} finally {
			inputStream.close();
		}
	}
}
