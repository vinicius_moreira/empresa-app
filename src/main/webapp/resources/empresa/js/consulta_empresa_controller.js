app.controller("consultaEmpresaController", function ($scope, $http, $location, $compile) {
	
	$scope.buscar = function(){
		$.ajax({
			method: "GET",
			async: false,
			url: $location.absUrl()+"/buscar",
			data: $("#form_busca").serialize(),
			success: function(resultado) {
				$scope.empresas = resultado;
			}
		});
	};

	$scope.excluir = function(url){
		$.ajax({
			method: "DELETE",
			async: false,
			url: url,
			success: function(resultado) {
				$scope.empresas = resultado;
			}
		});
	};

	$scope.buscar();
	
});