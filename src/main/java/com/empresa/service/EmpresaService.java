package com.empresa.service;

import java.util.List;

import org.springframework.data.domain.Pageable;


/**
 * Interface de serviços associados à empresas.
 * 
 * @author vinicius
 */
public interface EmpresaService {
	
	/**
	 * Busca uma empresa pelo seu identificador único.
	 * @param id
	 * 		o identificador único.
	 * @return
	 * 		a empresa encontrada ou <code>null<code> caso ela não esteja cadastrada.
	 * @throws NullPointerException
	 * 		quando o argumento é nulo.
	 */
	EmpresaDTO buscar(Long id) throws NullPointerException;

	/**
	 * Cria ou atualiza uma empresa no sistema.
	 * @param empresa
	 * 		os dados referentes à empresa.
	 * @return
	 * 		os dados persistidos.
	 * @throws NullPointerException
	 * 		quando o argumento é nulo.
	 */
	EmpresaDTO salvar(EmpresaDTO empresa) throws NullPointerException;
	
	/**
	 * Busca empresas.
	 * @param dadosBusca
	 * 		os dados à serem utilizados na busca.
	 * @param requisicaoPaginada
	 * 		os dados de requisição paginada
	 * @return
	 * 		o resultado paginado da busca
	 * @throws NullPointerException
	 * 		quando o argumento é nulo.
	 */
	List<EmpresaDTO> buscar(EmpresaDTO dadosBusca, Pageable requisicaoPaginada) throws NullPointerException;
	
	/**
	 * Remove uma empresa do repositório de dados.
     * @param id
     * 		o id da empresa
	 * @throws NullPointerException
	 * 		quando o argumento é nulo.
	 */
	void remover(Long id) throws NullPointerException;
}
