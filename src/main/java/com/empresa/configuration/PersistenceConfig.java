package com.empresa.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.dialect.HSQLDialect;
import org.hsqldb.jdbc.JDBCDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jolbox.bonecp.BoneCPDataSource;

/**
 * Configurações de persistência do Spring. Pode ser utililizado tanto para o contexto Root como para o Web, porém é altamente recomendável utilizar juntamente com o primeiro.
 * 
 * @author vinicius
 * @since X
 */
@Configuration("com.empresa.configuration.PersistenceConfig")
@EnableJpaRepositories(basePackages = "com.empresa.repository")
@EnableTransactionManagement // equivalente a <tx:annotation-driven transaction-manager="transactionManager"/>
@Lazy
//@PropertySource("classpath:application.properties") // substituído pelas constantes
public class PersistenceConfig {
	
	static final String DB_DRIVER_CLASS = JDBCDriver.class.getName();
	static final String DB_USERNAME = "sa";
	static final String DB_PASSWORD = "";
	static final String DB_JDBC_URL = "jdbc:hsqldb:mem://";
	
	static final Long DS_IDLE_CONNECTION_TEST_PERIOD_IN_MINUTES = 240L;
	static final Long DS_IDLE_MAX_AGE_IN_MINUTES = 240L;
	static final int DS_MAX_CONNECTIONS_PARTITION = 30;
	static final int DS_MIN_CONNECTIONS_PARTITION = 10;
	static final int DS_PARTITION_COUNT = 3;
	static final int DS_ACQUIRE_INCREMENT = 5;
	static final int DS_STATEMENT_CACHE_SIZE = 100;
	static final String ENTITY_PACKAGE = "com.empresa.model"; 
	
	static final String JPA_DIALECT = HSQLDialect.class.getName();
	static final String JPA_CONNECTION_CHARSET = "UTF-8";
	static final String JPA_DDL_MODE = "create-drop";
	static final boolean JPA_FORMAT_SQL = true;
	static final boolean JPA_SHOW_SQL = false;
	
	@Bean(destroyMethod="close")
	public DataSource dataSource(){
		BoneCPDataSource dataSource = new BoneCPDataSource();
		dataSource.setDriverClass(DB_DRIVER_CLASS);
		dataSource.setJdbcUrl(DB_JDBC_URL+this.getDatabaseName());
		dataSource.setUsername(DB_USERNAME);
		dataSource.setPassword(DB_PASSWORD);
		dataSource.setIdleConnectionTestPeriodInMinutes(DS_IDLE_CONNECTION_TEST_PERIOD_IN_MINUTES);
		dataSource.setIdleMaxAgeInMinutes(DS_IDLE_MAX_AGE_IN_MINUTES);
		dataSource.setMaxConnectionsPerPartition(DS_MAX_CONNECTIONS_PARTITION);
		dataSource.setMinConnectionsPerPartition(DS_MIN_CONNECTIONS_PARTITION);
		dataSource.setPartitionCount(DS_PARTITION_COUNT);
		dataSource.setAcquireIncrement(DS_ACQUIRE_INCREMENT);
		dataSource.setStatementsCacheSize(DS_STATEMENT_CACHE_SIZE);
		
		return dataSource;
	}
	
	@Bean
	@Autowired
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource){
		LocalContainerEntityManagerFactoryBean emFactoryBean = new LocalContainerEntityManagerFactoryBean();
		emFactoryBean.setDataSource(dataSource);
		emFactoryBean.setPackagesToScan(ENTITY_PACKAGE);
		emFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		emFactoryBean.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		emFactoryBean.setJpaPropertyMap(this.generateJpaProperties());
		
		return emFactoryBean;
	}
	
	@Bean
	@Autowired
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
		return new JpaTransactionManager(entityManagerFactory);
	}
	
	private Map<String, ?> generateJpaProperties(){
		Map<String, Object> jpaProperties = new HashMap<String, Object>();
		jpaProperties.put("hibernate.dialect", JPA_DIALECT);
		jpaProperties.put("hibernate.format_sql", JPA_FORMAT_SQL);
		jpaProperties.put("hibernate.show_sql", JPA_SHOW_SQL);
		jpaProperties.put("hibernate.connection.charSet", JPA_CONNECTION_CHARSET);
		jpaProperties.put("hibernate.hbm2ddl.auto", JPA_DDL_MODE);
		
		return jpaProperties;
	}
	
	public String getDatabaseName(){
		return "empresa";
	}
}