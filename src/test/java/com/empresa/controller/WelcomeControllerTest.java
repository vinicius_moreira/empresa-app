package com.empresa.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;

import com.empresa.TesteSpring;

/**
 * Testa a interface pública de {@link WelcomeControllerTest}.
 * 
 * @author vinicius
 */
public class WelcomeControllerTest extends TesteSpring {

	@Test
	public void testWelcome () throws Exception {
		ResultActions result = this.mockMvc.perform(get("/"));
		
		result.andExpect(status().isFound());
		result.andExpect(view().name("redirect:/empresas/listagem"));
	}
}
