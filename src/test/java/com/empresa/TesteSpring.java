package com.empresa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.web.WebDelegatingSmartContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.empresa.configuration.PersistenceConfigTesting;
import com.empresa.configuration.RootConfig;
import com.empresa.configuration.WebConfig;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;

/**
 * Class mãe dos testes unitários do sistema.
 * 
 * @author vinicius
 * @since X
 */
@ContextHierarchy({
	@ContextConfiguration(classes = {RootConfig.class, PersistenceConfigTesting.class}),
	@ContextConfiguration(classes = WebConfig.class, loader = WebDelegatingSmartContextLoader.class)
})
@DbUnitConfiguration(dataSetLoader=CustomDatasetLoader.class)
@RunWith(value=SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DirtiesContextTestExecutionListener.class,
    DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })
@TransactionConfiguration(transactionManager="transactionManager")
@WebAppConfiguration
public abstract class TesteSpring {

	@PersistenceContext
	protected EntityManager em;
	
	@Autowired
	private WebApplicationContext wac;
	
	protected MockMvc mockMvc;
	
	@Before
	public void setUp(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
}