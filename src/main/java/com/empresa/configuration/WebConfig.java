package com.empresa.configuration;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.support.ControllerClassNameHandlerMapping;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

/**
 * Configurações para o contexto Web do Spring.
 * 
 * @author vinicius
 * @since X
 */
@Configuration("com.empresa.configuration.WebContext")
@ComponentScan("com.empresa.controller")
@EnableWebMvc
@Lazy
public class WebConfig extends WebMvcConfigurerAdapter {
	
	static final String VIEW_CONTENT_TYPE = "text/html; charset=UTF-8";

	/* INÍCIO: mapeamento de recursos Web (css, javascript,...) */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}
	/* FIM: mapeamento de recursos Web (css, javascript,...) */
	
	/*--- Início: Configurações de i18n ---*/
	@Bean
	public SessionLocaleResolver localeResolver(){
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(new Locale("pt", "BR"));
		return localeResolver;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor(){
		LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
		localeInterceptor.setParamName("language");
		return localeInterceptor;
	}
	
	@Bean
	public MessageSource messageSource(){
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}
	
	@Bean
	@Autowired
	public ControllerClassNameHandlerMapping controllerClassNameHandlerMapping(LocaleChangeInterceptor localeChangeInterceptor){
		ControllerClassNameHandlerMapping controllerMapping  = new ControllerClassNameHandlerMapping();
		controllerMapping.setInterceptors(new Object[]{localeChangeInterceptor});
		return controllerMapping;
	}
	
	/*--- Fim: Configurações de i18n ---*/
	
	
	/*--- Início: Configurações Velocity ---*/
	@Bean
	public VelocityConfigurer velocityConfig(){
		VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
		velocityConfigurer.setResourceLoaderPath("/WEB-INF/views");
		
		return velocityConfigurer;
	}
	
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		VelocityViewResolver velocityResolver = new VelocityViewResolver();
		velocityResolver.setOrder(1);
		velocityResolver.setPrefix("_");
		velocityResolver.setSuffix(".vm");
		velocityResolver.setContentType(VIEW_CONTENT_TYPE);
		velocityResolver.setCache(true);
		registry.viewResolver(velocityResolver);
		
		VelocityLayoutViewResolver velocityLayoutResolver = new VelocityLayoutViewResolver();
		velocityLayoutResolver.setOrder(2);
		velocityLayoutResolver.setPrefix("");
		velocityLayoutResolver.setSuffix(".vm");
		velocityLayoutResolver.setContentType(VIEW_CONTENT_TYPE);
		velocityLayoutResolver.setCache(true);
		registry.viewResolver(velocityLayoutResolver);
	}
	/*--- Fim: Configurações Velocity ---*/
}
