package com.empresa.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.Validate;

@Entity
@Table(name="EMP_EMPRESAS")
public class EEmpresa implements Serializable {

	
	/** */
	private static final long serialVersionUID = -5467324681166278866L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="RAZAO",nullable=false,length=100)
	private String razaoSocial;
	
	@Column(unique=true, nullable=false, length=18)
	private String cnpj;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="empresa", cascade = CascadeType.ALL, orphanRemoval = true)
	private EEndereco endereco;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="MATRIZ_ID")
	private EEmpresa matriz;
	
	@OneToMany(mappedBy="matriz", fetch=FetchType.LAZY,cascade=CascadeType.REMOVE,orphanRemoval=true)
	private Set<EEmpresa> filiais;
	
	@Column(name="TEL_PRI", nullable=false)
	private String telefonePrincipal;
	
	@Column(name="TEL_SEC")
	private String telefoneSecundario;
	
	@Column
	private String fax;
	
	@Column
	private String email;
	
	@Lob
	@Column
	private byte[] logo;
	
	@Lob
	@Column(name="CSOCIAL")
	private byte[] contratoSocial;
	
	
	/**
	 * @deprecated
	 * 		deve ser utilizado somente pelo JPA.
	 */
	@Deprecated
	private EEmpresa(){
		//
	}
	
	
	/**
	 * Constrói uma nova instância de {@link EEmpresa}.
	 * @param razaoSocial
	 * @param cnpj
	 * @param telefonePrincipal
	 * @throws NullPointerException
	 * 		quando os argumentos são nulos.
	 */
	public EEmpresa(String razaoSocial, String cnpj, String telefonePrincipal) throws NullPointerException {
		this();
		Validate.notNull(razaoSocial);
		Validate.notNull(cnpj);
		Validate.notNull(telefonePrincipal);
		this.razaoSocial = razaoSocial;
		this.cnpj = cnpj;
		this.telefonePrincipal = telefonePrincipal;
	}

	public boolean isMatriz(){
		return this.matriz == null;
	}
	
	public boolean isFilial(){
		return this.matriz != null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public EEndereco getEndereco() {
		return endereco;
	}

	public void setEndereco(EEndereco endereco) {
		this.endereco = endereco;
	}

	public EEmpresa getMatriz() {
		return matriz;
	}

	public void setMatriz(EEmpresa matriz) {
		this.matriz = matriz;
	}
	
	public Set<EEmpresa> getFiliais() {
		return filiais;
	}

	public void setFiliais(Set<EEmpresa> filiais) {
		this.filiais = filiais;
	}

	public String getTelefonePrincipal() {
		return telefonePrincipal;
	}

	public void setTelefonePrincipal(String telefonePrincipal) {
		this.telefonePrincipal = telefonePrincipal;
	}

	public String getTelefoneSecundario() {
		return telefoneSecundario;
	}

	public void setTelefoneSecundario(String telefoneSecundario) {
		this.telefoneSecundario = telefoneSecundario;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public byte[] getContratoSocial() {
		return contratoSocial;
	}

	public void setContratoSocial(byte[] contratoSocial) {
		this.contratoSocial = contratoSocial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EEmpresa other = (EEmpresa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EEmpresa [id=" + id + ", razaoSocial=" + razaoSocial
				+ ", cnpj=" + cnpj + "]";
	}

}