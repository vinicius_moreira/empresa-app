package com.empresa.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Lazy;

/**
 * Configurações para o contexto Root do Spring.
 * 
 * @author vinicius
 * @since X
 */
@Configuration("com.empresa.configuration.RootContext")
@ComponentScan(
		basePackages={"com.empresa"},
		excludeFilters={@Filter(type=FilterType.REGEX, pattern={"com.empresa.controller.*","com.empresa.configuration.*"})}
)
@Lazy
public class RootConfig {
	
}
