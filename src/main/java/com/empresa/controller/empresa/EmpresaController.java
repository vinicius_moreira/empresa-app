package com.empresa.controller.empresa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empresa.service.EmpresaDTO;
import com.empresa.service.EmpresaService;

@Controller("com.empresa.controller.empresa.EmpresaController")
@RequestMapping("/empresas")
public class EmpresaController {

	private static final int REGISTROS_PAGINA = 20;
	
	@Autowired
	private EmpresaService empresaService;
	
	@Resource
	private MessageSource messages;

	/**
	 * @return a view associada à inclusão de novas empresas.
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView incluir() {
		return new ModelAndView("empresa/inclusao");
	}

	/**
	 * @param id
	 * 		o id da empresa à ser editada
	 * @return
	 * 		a view associada à edição de empresas
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ModelAndView atualizar(@PathVariable Long id) {

		EmpresaDTO empresa = this.empresaService.buscar(id);
		Map<String, Object> model = new HashMap<String, Object>();

		if (empresa != null) {
			model.put("empresa", empresa);
		}

		return new ModelAndView("empresa/inclusao",model);
	}
	
	/**
	 * @param id
	 * 		o id da empresa à ser editada
	 * @return
	 * 		a view associada à edição de empresas
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public @ResponseBody List<EmpresaDTO> excluir(@PathVariable Long id) {

		this.empresaService.remover(id);
		
		return this.buscar("", "", "", "");
	}

	/**
	 * Cria ou atualiza uma empresa no repositório de dados.
	 * @param empresa
	 * 		a empresa alvo.
	 * @return
	 * 		a view responsável por listar todas as empresas.
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String salvar(EmpresaDTO empresa, final RedirectAttributes redirectAttributes) {
		String msgCode = empresa.getId() == null ? "emp.inc.msg.salvar.sucesso" : "emp.inc.msg.atualizar.sucesso";

		this.empresaService.salvar(empresa);
		
		redirectAttributes.addFlashAttribute("msg", messages.getMessage(msgCode, null, LocaleContextHolder.getLocale()));
		
		return "redirect:empresas/listagem";
	}

	/**
	 * @return
	 * 		a view responsável por listar todas as empresas.
	 */
	@RequestMapping(value = "listagem", method = RequestMethod.GET)
	public ModelAndView listar() {
		
		return new ModelAndView("empresa/listagem");
	}
	
	@RequestMapping(value="listagem/buscar", method = RequestMethod.GET)
	public @ResponseBody List<EmpresaDTO> buscar(@RequestParam String cnpj, @RequestParam String razao, @RequestParam String municipio, @RequestParam String uf){
		
		EmpresaDTO dadosBusca = new EmpresaDTO();
		dadosBusca.setCnpj(cnpj);
		dadosBusca.setRazao(razao);
		dadosBusca.setMunicipio(municipio);
		dadosBusca.setUf(uf);
		
		return this.empresaService.buscar(dadosBusca, new PageRequest(0, REGISTROS_PAGINA));
	}

}
