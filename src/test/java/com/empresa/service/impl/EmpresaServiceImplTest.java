package com.empresa.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.empresa.Dataset;
import com.empresa.TesteSpring;
import com.empresa.model.EEmpresa;
import com.empresa.repository.EmpresaRepository;
import com.empresa.repository.EnderecoRepository;
import com.empresa.service.EmpresaDTO;
import com.empresa.service.EmpresaService;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

public class EmpresaServiceImplTest extends TesteSpring {
	
	
	@Autowired
	private EmpresaService empresaService;
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testSalvar$empresaValida(){
		
		String razao = "Nova Empresa";
		String cnpj = "25914176000180";
		String logradouro = "Rua Z";
		Integer numero = 14;
		String complemento = "Edífico Colinas, Sala 12";
		String municipio = "São Sebastião";
		String uf = "SP";
		String telp = "+551266667777";
		String tels = "+5512333344444";
		String fax = "+551255559999";
		String email = "novaempresa@x.com";
		
		EmpresaDTO empresa = new EmpresaDTO();
		empresa.setRazao(razao);
		empresa.setCnpj(cnpj);
		empresa.setLogradouro(logradouro);
		empresa.setNumero(numero);
		empresa.setComplemento(complemento);
		empresa.setMunicipio(municipio);
		empresa.setUf(uf);
		empresa.setTelp(telp);
		empresa.setTels(tels);
		empresa.setFax(fax);
		empresa.setEmail(email);
		
		empresa = this.empresaService.salvar(empresa);
		assertNotNull(empresa.getId());
		
		this.em.flush();
		this.em.clear();
		
		assertEquals(3, this.empresaRepository.count());
		assertEquals(3, this.enderecoRepository.count());
		
		EEmpresa entidade = this.empresaRepository.findOne(empresa.getId());
		assertNotNull(entidade);
		assertEquals(razao, entidade.getRazaoSocial());
		assertEquals(cnpj, entidade.getCnpj());
		assertEquals(telp, entidade.getTelefonePrincipal());
		assertEquals(tels, entidade.getTelefoneSecundario());
		assertEquals(fax, entidade.getFax());
		assertEquals(email, entidade.getEmail());
		
		assertNotNull(entidade.getEndereco());
		assertEquals(logradouro, entidade.getEndereco().getLogradouro());
		assertEquals(numero, entidade.getEndereco().getNumero());
		assertEquals(municipio, entidade.getEndereco().getMunicipio());
		assertEquals(complemento, entidade.getEndereco().getComplemento());
		assertEquals(uf, entidade.getEndereco().getUf());
	}
	
	@Test
	@Transactional
	@DatabaseSetup(Dataset.CENARIO_01)
	@ExpectedDatabase(Dataset.CENARIO_01_03)
	@DatabaseTearDown(Dataset.VAZIO)
	public void testSalvar$atualizarEmpresa(){
		EmpresaDTO empresa = new EmpresaDTO();
		empresa.setId(1L);
		empresa.setRazao("Nova Google");
		empresa.setCnpj("20674156000165");
		empresa.setLogradouro("Rua W");
		empresa.setNumero(15);
		empresa.setComplemento("Sala 16");
		empresa.setMunicipio("Rio de Janeiro");
		empresa.setUf("RJ");
		empresa.setTelp("+551266667778");
		empresa.setTels("+5512333344445");
		empresa.setFax("+551255559998");
		empresa.setEmail("google@gmail.com");
		empresa = this.empresaService.salvar(empresa);
	}
}