package com.empresa.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Extensão de {@link PersistenceConfig} para os testes unitários.<br/><br/>
 * Obs.: atualmente só redefine o nome da base de dados em memória.
 * 
 * @author vinicius
 * @since X
 */
@Configuration("com.empresa.configuration.PersistenceConfigTesting")
@Lazy
public class PersistenceConfigTesting extends PersistenceConfig {
	
	@Override
	public String getDatabaseName(){
		return "empresa_teste";
	}
}