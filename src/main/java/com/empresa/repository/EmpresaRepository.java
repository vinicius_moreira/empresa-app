package com.empresa.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.empresa.model.EEmpresa;

/**
 * {@link CrudRepository} para {@link EEmpresa}
 * 
 * @author vinicius
 */
public interface EmpresaRepository extends CrudRepository<EEmpresa, Long> {
	
	Page<EEmpresa> findByCnpjLikeAndRazaoSocialLikeAndEnderecoMunicipioLikeAndEnderecoUfLikeAllIgnoreCase(String cnpj, String razaoSocial, String municipio, String uf, Pageable pageable);

}
