package com.empresa.service.impl;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.empresa.model.EEmpresa;
import com.empresa.model.EEndereco;
import com.empresa.service.EmpresaDTO;

@Component("com.empresa.service.impl.ConversorEntidades")
public class ConversorEntidades {

	/**
	 * Adiciona os dados de um {@link EmpresaDTO} para um {@link EEmpresa}.
	 * @param dto
	 * @param entidade
	 * @throws NullPointerException
	 * 		caso os argumentos sejam nulos.
	 */
	public void popularEmpresa(EmpresaDTO dto, EEmpresa entidade) throws NullPointerException{
		Validate.notNull(dto, "EmpresaDTO nulo !");
		Validate.notNull(entidade,"EEmpresa nulo !");
		
		entidade.setRazaoSocial(dto.getRazao());
		entidade.setCnpj(dto.getCnpj());
		
		EEndereco endereco = entidade.getEndereco();
		
		if(endereco == null){
			endereco = new EEndereco(entidade, dto.getLogradouro(), dto.getNumero(), dto.getMunicipio(), dto.getUf());
			entidade.setEndereco(endereco);
		}
		else{
			endereco.setLogradouro(dto.getLogradouro());
			endereco.setNumero(dto.getNumero());
			endereco.setMunicipio(dto.getMunicipio());
			endereco.setUf(dto.getUf());
		}
		
		endereco.setComplemento(dto.getComplemento());
		
		entidade.setTelefonePrincipal(dto.getTelp());
		entidade.setTelefoneSecundario(dto.getTels());
		entidade.setFax(dto.getFax());
		entidade.setEmail(dto.getEmail());
		
		entidade.setLogo(dto.getLogo());
		entidade.setContratoSocial(dto.getContrato());
	}
	
	/**
	 * Adiciona os dados de um {@link EEmpresa} para um {@link EmpresaDTO}.
	 * @param entidade
	 * @param dto
	 * @throws NullPointerException
	 * 		caso os argumentos sejam nulos.
	 */
	@Transactional(readOnly=true, propagation = Propagation.MANDATORY)
	public void popularDTO(EEmpresa entidade, EmpresaDTO dto) throws NullPointerException{
		Validate.notNull(entidade,"EEmpresa nulo !");
		Validate.notNull(dto, "EmpresaDTO nulo !");
		
		dto.setId(entidade.getId());
		dto.setRazao(entidade.getRazaoSocial());
		dto.setCnpj(entidade.getCnpj());
		
		EEndereco endereco = entidade.getEndereco();
		
		if(endereco != null){
			dto.setLogradouro(endereco.getLogradouro());
			dto.setNumero(endereco.getNumero());
			dto.setComplemento(endereco.getComplemento());
			dto.setMunicipio(endereco.getMunicipio());
			dto.setUf(endereco.getUf());
		}
		
		dto.setTelp(entidade.getTelefonePrincipal());
		dto.setTels(entidade.getTelefoneSecundario());
		dto.setFax(entidade.getFax());
		dto.setEmail(entidade.getEmail());
		dto.setLogo(entidade.getLogo());
		dto.setContrato(entidade.getContratoSocial());
	}
}
